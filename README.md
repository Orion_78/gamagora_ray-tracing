2014 - Gamagora, Lyon

Ray tracing on spheres done with Guillaume Bouchard

Using Multithreading with omp

```
#!c++

#pragma omp parallel for num_threads(8) shared(done)
```



Result with 30 samples per pixel and 8 ray bounces, 

Using Diffuse on the wall (which are in fact hugh spheres), Refraction on the little left sphere and Specular on the little right sphere. The white sphere in the middle is the light source.

You can see refraction for the right sphere on the bottom.

![Sans titre.jpg](https://bitbucket.org/repo/6eKgep/images/4025011147-Sans%20titre.jpg)