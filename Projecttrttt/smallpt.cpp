// This code is highly based on smallpt
// http://www.kevinbeason.com/smallpt/
// DOC : http://people.cs.kuleuven.be/~philip.dutre/GI/TotalCompendium.pdf

#include <math.h>		// smallpt, a Path Tracer by Kevin Beason, 2008
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <random>
#include <map>

//#define M_PI 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196442881097566593344612847564823378678316527120190914564856692346034861045432664821339360726024914127372458700660631558817488152092096282925409171536436789259036001133053054882046652138414695194151160943305727036575959195309218611738193261179310511854807446237996274956735188575272489122793818301194912983367336244065664308602139494639522473719070217986094370277053921717629317675238467481846766940513200056812714526356082778577134275778960917363717872146844090122495343014654958537105079227968;
#define _CRT_SECURE_NO_WARNINGS 1
#define BOUNCE_COUNT 8
#define SAMPLE 30
#define SPHERELIGHT
//#define REMOVEBORDERCOLOR // time ~ * 1/4

typedef double UNIT;

UNIT M_PI = 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513282306647093844;

struct Vec
{
	UNIT x, y, z;		// position, also color (r,g,b)
	explicit Vec (UNIT x_ = 0, UNIT y_ = 0, UNIT z_ = 0)
	{
		x = x_;
		y = y_;
		z = z_;
	}
	Vec operator+ (const Vec & b) const
	{
		return Vec (x + b.x, y + b.y, z + b.z);
	}
	Vec operator- (const Vec & b) const
	{
		return Vec (x - b.x, y - b.y, z - b.z);
	}
	Vec operator* (UNIT b) const
	{
		return Vec (x * b, y * b, z * b);
	}
	Vec mult (const Vec & b) const
	{
		return Vec (x * b.x, y * b.y, z * b.z);
	}
	Vec div (const Vec & b) const
	{
		return Vec (x / b.x, y / b.y, z / b.z);
	}
	Vec div (const UNIT b) const
	{
		return Vec (x / b, y / b, z / b);
	}
	Vec & norm ()
	{
		return *this = *this * (1 / sqrt (x * x + y * y + z * z));
	}
	UNIT dot (const Vec & b) const
	{
		return x * b.x + y * b.y + z * b.z;
	}				// cross:
	Vec operator% (Vec & b)
	{
		return Vec (y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
	}

	UNIT length() const
	{
		return std::sqrt(x * x + y * y + z * z);
	}

	UNIT distance(const Vec &one, const Vec &two)
	{
		return (one - two).length();
	}
};

struct Ray
{
	Vec o, d;
	Ray (Vec o_, Vec d_):o (o_), d (d_)
	{
	}
};

enum Refl_t
{
	DIFFUSE,
	SPECULAIRE,
	REFRACTION,
	LIGHT
};

struct Sphere
{
	UNIT rad;			// radius
	Vec p, e, c;			// position, emission, color
	Refl_t refl;			// reflection type (DIFFuse, SPECular, REFRactive)
	Sphere (UNIT rad_, Vec p_, Vec e_, Vec c_, Refl_t refl_):
		rad (rad_),
		p (p_), e (e_), c (c_), refl (refl_)
	{}

	// WARRING: works only if r.d is normalized
	UNIT intersect (const Ray & r) const
	{				// returns distance, 0 if nohit
		Vec op = p - r.o;		// Solve t^2*d.d + 2*t*(o-p).d + (o-p).(o-p)-R^2 = 0
		UNIT t, b = op.dot (r.d), det =
			b * b - op.dot (op) + rad * rad;
		if (det < 0)
			return 0;
		else
			det = sqrt (det);
		return (t = b - det) >= 0 ? t : ((t = b + det) >= 0 ? t : 0);
	}
};

//Vec light(50, 70, 81.6);
Sphere light(10, Vec(50, 50, 50), Vec (), Vec (1000, 1000, 1000) , LIGHT);

Sphere spheres[] = {		//Scene: radius, position, emission, color, material
	Sphere (1e5, Vec (1e5 + 1, 40.8, 81.6), Vec (), Vec (.75, .25, .25), DIFFUSE),	//Left
	Sphere (1e5, Vec (-1e5 + 99, 40.8, 81.6), Vec (), Vec (.25, .25, .75), DIFFUSE),	//Rght
	Sphere (1e5, Vec (50, 40.8, 1e5), Vec (), Vec (.75, .75, .75), DIFFUSE),	//Back
	Sphere (1e5, Vec (50, 40.8, -1e5 + 170), Vec (), Vec (), DIFFUSE),	//Frnt
	Sphere (1e5, Vec (50, 1e5, 81.6), Vec (), Vec (.75, .75, .75), DIFFUSE),	//Botm
	Sphere (1e5, Vec (50, -1e5 + 81.6, 81.6), Vec (), Vec (.75, .75, .75), DIFFUSE),	//Top
	Sphere (16.5, Vec (27, 16.5, 47), Vec (), Vec (1, 1, 1) * .999, SPECULAIRE),	//Mirr SPECULAIRE
	Sphere (16.5, Vec (73, 16.5, 78), Vec (), Vec (1, 1, 1) * .999, REFRACTION),	//Glas REFRACTION
#ifdef SPHERELIGHT
	light
#endif
};



inline UNIT
	clamp (UNIT x)
{
	return x < 0 ? 0 : x > 1 ? 1 : x;
}

std::default_random_engine generator;
std::uniform_real_distribution<UNIT> distribution(0.0,1.0);

UNIT random_u()
{
	return distribution(generator);
}

Vec sample_cos(UNIT u, UNIT v, Vec n)
{
	// Ugly: create an ornthogonal base
	Vec basex, basey, basez;

	basez = n;
	basey = Vec(n.y, n.z, n.x);

	basex = basez % basey;
	basex.norm();

	basey = basez % basex;

	// cosinus sampling. Pdf = cosinus
	return  basex * (std::cos(2.f * M_PI * u) * std::sqrt(1 - v)) +
		basey * (std::sin(2.f * M_PI * u) * std::sqrt(1 - v)) +
		basez * std::sqrt(v);
}

inline int toInt (UNIT x)
{
	return int (pow (clamp (x), 1 / 2.2) * 255 + .5);
}

// WARNING: ASSUME NORMALIZED RAY
// Compute the intersection ray / scene.
// Returns true if intersection
// t is defined as the abscisce along the ray (i.e
//             p = r.o + t * r.d
// id is the id of the intersected object
inline bool intersect (const Ray & r, UNIT &t, int &id)
{
	UNIT n = sizeof (spheres) / sizeof (Sphere), d, inf = t = 1e20;
	for (int i = int (n); i--;)
		if ((d = spheres[i].intersect (r)) && d < t)
		{
			t = d;
			id = i;
		}
		return t < inf;
}

// return true if there is an object between r.o and r.o + tmax * r.d
inline bool occlude (const Ray & r, UNIT tmax)
{
	UNIT n = sizeof (spheres) / sizeof (Sphere), d;
	for (int i = int (n); i--;)
		if ((d = spheres[i].intersect (r)) && d < tmax)
		{
			return true;
		}
		return false;
}

// Reflect the ray i along the normal.
// i should be oriented as "leaving the surface"
Vec reflect(const Vec i, const Vec n)
{
	return n * (n.dot(i)) * 2.f - i;
}

UNIT  sin2cos (UNIT x)
{
	return std::sqrt(std::max(0.0, 1.0f-x*x));
}

// Fresnel coeficient of transmission.
// Normal point outside the surface
// ior is n0 / n1 where n0 is inside and n1 is outside
UNIT fresnelR(const Vec i, Vec n, UNIT ior)
{
	if(n.dot(i) < 0)
	{
		n = n * -1.f;
		ior = 1.f / ior;
	}

	UNIT R0 = (ior - 1.f) / (ior + 1.f);
	R0 *= R0;

	return R0 + (1 - R0) * std::pow(1.f - i.dot(n), 5.f);
}

// compute refraction vector.
// return true if refraction is possible.
// i and n are normalized
// output wo, the refracted vector (normalized)
// n point oitside the surface.
// ior is n00 / n1 where n0 is inside and n1 is outside
bool refract(Vec i, Vec n, UNIT ior, Vec &wo)
{
	i = i * -1.f;

	if(n.dot(i) > 0)
	{
		n = n * -1.f;
	}
	else
	{
		ior = 1.f / ior;
	}

	UNIT k = 1.f - ior * ior * (1.f - n.dot(i) * n.dot(i));
	if (k < 0.f)
		return false;

	wo = i * ior - n * (ior * n.dot(i) + std::sqrt(k));

	return true;
}

/*
* Select a light point from a hemispherique light
* TODO Sample only on visible sphere light
*/
Vec selectLightPoint(Vec impactPoint)
{
#ifdef SPHERELIGHT
	UNIT r1 = random_u();
	UNIT r2 = random_u();

	Vec pointLight = Vec(
		cos(2*M_PI*r1)*sqrt( 1 - pow( r2 , 2 ) ),
		sin(2*M_PI*r1)*sqrt( 1 - pow( r2 , 2 ) ),
		r2 );

	Vec dirLight = ( impactPoint - light.p ).norm();

	// 1.01f : avoid point inside sphere
	pointLight = (pointLight + dirLight).norm();

	// 1.1f : Avoid vector null if direction are opposite
	pointLight = pointLight*light.rad*1.1f + light.p;


	return pointLight;
#else
	return light.p;
#endif
}

/*

BSDF = facteur de contributation de la matiere


* return the color emited by one ray
*/
Vec radiance (const Ray & r, int bounce, bool diffuseHit = false)
{
	UNIT t;
	int id;

	// this should not happen
	if ( !intersect(r,t,id) )
	{
		return Vec(0.5,0,0);
	}

	Vec impactPos = r.o + r.d*t;

	Vec lightPoint = selectLightPoint(impactPos);

	Vec dirToLight = (lightPoint - impactPos).norm();
	// *0.1 to get out of the object
	Ray dirToLightRay(impactPos + dirToLight*0.1 , dirToLight);
	UNIT distance = (lightPoint - impactPos).length();

	Vec normalSphere = ( impactPos - spheres[id].p ).norm();

	if (bounce <= 0)
		return Vec(0,0,0);
	bounce--;


	// switch materials
	switch (spheres[id].refl)
	{
	case Refl_t::SPECULAIRE:
		{
			Vec ReflectionDir = reflect( r.d*(-1), normalSphere );
			Ray reflet( impactPos + ReflectionDir * 0.1, ReflectionDir );
			return spheres[id].c.mult( radiance( reflet,bounce, diffuseHit ) );
		}
		// Diffuse with bouncing light
	case Refl_t::DIFFUSE:
		{
			Vec myColor;

			// Because all wall are reversed, this will occure really often
			// -1 because ray have to come out of the sphere
			if (normalSphere.dot(r.d * -1) < 0)
			{
				//printf("ERROR : normal negative");
				normalSphere = normalSphere * -1;
			}


			/*
			* Eclairage direct
			*/
			Vec inversedirToLightRay = dirToLightRay.d * (-1);
			if (occlude(dirToLightRay, distance) || inversedirToLightRay.dot(normalSphere) > 0)
			{
				myColor = Vec(0,0,0);
			}
			else
			{



				UNIT thetaDiffuse = inversedirToLightRay.dot( normalSphere );

				// Avoid realy small distance
				UNIT distanceCarre;
				if (distance < 1 )
					distanceCarre = 1;
				else
				distanceCarre = pow(distance,2);

				UNIT BSDFDiffuse = (abs(thetaDiffuse) / M_PI);

				Vec lightPowa = light.c.div(  distanceCarre );


				myColor = spheres[id].c.mult( lightPowa ) * BSDFDiffuse ;

				// Uncorrect
				//if ( myColor.length()  > 1)
				//myColor = myColor.norm();
			}


			Vec randomReflectDir = sample_cos( random_u(), random_u(), normalSphere);
			Ray randomReflectRay = Ray( impactPos + randomReflectDir * 0.1, randomReflectDir );
			/*
			* intersect is too much consuming
			intersect( rayReflect ,t,id);
			if ( spheres[id].refl != Refl_t::LIGHT)
			{
			return myColor + ( radiance( rayReflect ,bounce ) * normalSphere.dot(randomReflect) ).div(M_PI) ;
			}
			else
			{
			return myColor ;
			}*/

			// BSDF / cos thete / PI


			// proba d'un rayon sur l'h�misphere avec plus de proba au cot� de la normal cos theta / PI
			// depuis samplecod

			return myColor + spheres[id].c.mult( radiance( randomReflectRay , bounce, true ) )  ; //* normalSphere.dot(randomReflect) ).div(M_PI) simplifier
		}
	case Refl_t::REFRACTION:
		{
			// MIROIR
			Vec ReflectionDir = reflect( r.d*(-1), normalSphere );
			Ray reflet( impactPos + ReflectionDir * 0.1f, ReflectionDir );
			Vec colorReflet = spheres[id].c.mult( radiance( reflet,bounce, diffuseHit ) );


			// Frenet power
			UNIT fres = fresnelR(r.d * (-1), normalSphere, 1.5);
			colorReflet = colorReflet * fres;

			Vec out;
			bool refraction = refract(r.d *(-1) ,normalSphere, 1.5, out);

			if (refraction)
			{
				//  - ReflectionDir because inside refraction sphere
				return colorReflet + spheres[id].c.mult( radiance( Ray( impactPos - ReflectionDir * 0.1,out ) ,bounce, diffuseHit) ) * (1 - fres);
			}
			else
			{
				return colorReflet;
			}

		}
	case Refl_t::LIGHT:

		if ( diffuseHit ) 
		{
			return Vec(0,0,0);
		}
		else
		{
			return light.c;
		}

	default:
		return Vec(0.5,0,0);

	}
}



int main (int argc, char *argv[])
{
	int w = 1024, h = 768, samps = argc == 2 ? atoi (argv[1]) / 4 : SAMPLE;	// # samples
	Ray cam (Vec (50, 52, 295.6), Vec (0, -0.042612, -1).norm ());	// cam pos, dir
	Vec cx = Vec (w * .5135 / h), cy = (cx % cam.d).norm () * .5135, *c =
		new Vec[w * h];

	int done = 0;
	double currentPct = 0;
	// Multi threading on eight processor
#pragma omp parallel for num_threads(8) shared(done)
	for (int y = 0; y < h; y++)
	{				// Loop over image rows


		if (currentPct < 100. * done / (h - 1))
		{
			currentPct = 100. * done / (h - 1) + 10;
			fprintf (stderr, "\rRendering (%d spp) %5.2f%%", samps * 4,
				100. * done / (h - 1));
		}

		for (unsigned short x = 0; x < w; x++)	// Loop cols
		{
#ifdef REMOVEBORDERCOLOR
			int indexsamplesResult = 0;
#endif
			
			for (int sy = 0, i = (h - y - 1) * w + x; sy < 2; sy++)	// 2x2 subpixel rows
			{
#ifdef REMOVEBORDERCOLOR
				Vec samplesResult[SAMPLE*4];
#endif

				for (int sx = 0; sx < 2; sx++)
				{			// 2x2 subpixel cols
					Vec r = Vec ();
					for (int s = 0; s < samps; s++)
					{
						UNIT dx = 0;
						UNIT dy = 0;

						Vec d =
							cx * (((sx + .5 + dx) / 2 + x) / w - .5) +
							cy * (((sy + .5 + dy) / 2 + y) / h - .5) + cam.d;
						int a = BOUNCE_COUNT;

#ifdef REMOVEBORDERCOLOR
						samplesResult[indexsamplesResult] = radiance (Ray (cam.o + d * 140, d.norm ()), a);
						indexsamplesResult++;
#else
						r =
						r + radiance (Ray (cam.o + d * 140, d.norm ()), a) * (1. / samps);
#endif

						
					}		// Camera rays are pushed ^^^^^ forward to start in interior

#ifndef REMOVEBORDERCOLOR
					c[i] = c[i] + Vec (clamp (r.x), clamp (r.y), clamp (r.z)) * .25;
#endif
					



				}

				/*
				// Remove two long point from a medium range
				UNIT moy = 0;

				for(int i =1; i<SAMPLE*4; i++)
				{
				UNIT f = samplesResult[i].distance(samplesResult[i],samplesResult[0]);
				moy += f / (SAMPLE*4);
				}

				for(int i =0; i<SAMPLE*4; i++)
				{
				if (samplesResult[i].length() > moy)
				{

				}
				}
				*/


#ifdef REMOVEBORDERCOLOR
				std::map<UNIT,Vec> trier;
				int pointToStart = random_u() * SAMPLE*4;
				for(int current =0; current<SAMPLE*4; current++)
				{
					if (current != pointToStart)
						trier[ samplesResult[current].distance(samplesResult[current],samplesResult[pointToStart] ) ] = samplesResult[current];
				}


				std::map<UNIT,Vec>::iterator it = trier.end();
				// Remove x pct of points
				for (int ii = 0; ii < SAMPLE*4 * 0.1 ; ii++)
				{
					--it;
				}
				
				trier.erase(it, trier.end());


				for (std::map<UNIT,Vec>::iterator it = trier.begin(); it != trier.end() ; ++it)
				{
					c[i] = c[i] + (*it).second .div( trier.size() );
				}
#endif
				
			}
			
		}
		done++;

	}
	FILE *f;
	fopen_s (&f, "image.ppm", "w");	// Write image to PPM file.
	fprintf (f, "P3\n%d %d\n%d\n", w, h, 255);
	for (int i = 0; i < w * h; i++)
		fprintf (f, "%d %d %d ", toInt (c[i].x), toInt (c[i].y), toInt (c[i].z));
}
